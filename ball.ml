(* To compile this example: ocamlc graphics.cma unix.cma ball.ml -o ball *)
open Graphics;;
open List;;
open Unix;;
open Printf;;

open_graph " 640x480";;

(* Define as { x = 1; y = 2 } *)
type vector_2d = { x: int; y: int };;

let leading_point point velocity =
    (* Determine which of the four corners 
     * of the ball is the leading point and
     * return it. *)
  if velocity. x > 0 && velocity. y > 0 then
    { x = point. x + 20; y = point. y + 20 }

  else if velocity. x > 0 then
    { x = point. x + 20; y = point. y }

  else if velocity. y > 0 then
    { x = point. x; y = point. y + 20 }

  else
    { x = point. x; y = point. y }
  

let draw_ball point =
  (* Clear the graph and
     Draw the ball. *)
  clear_graph ();
  set_color magenta;
  fill_rect point.x point.y 20 20;;

let bounce point velocity =
  (* note: we're passing an invalid point 
     to this function and it is returning a valid one.

     Calculate the new position of a bounced ball. 
     Return a list of the new point and new velocity. *)
  let lead_point = leading_point point velocity in
  let bounce_point = {
      x = if lead_point. x > 640 then
            point. x - (lead_point. x - 640)
          else if lead_point. x < 0 then
            0 - point. x
          else
            point. x;

      y = if lead_point. y > 480 then
            point. y - (lead_point. y - 480)
          else if lead_point. y < 0 then
            0 - point. y
          else
            point. y
    } in
  let bounce_velocity = {
      x = if bounce_point. x != point. x then  
            velocity. x * -1
          else
            velocity. x;

      y = if bounce_point. y != point. y then  
            velocity. y * -1
          else
            velocity. y;
    } in
  [bounce_point; bounce_velocity];;

let rec animate point velocity =
  draw_ball point;

  sleepf 0.1;

  (* If the next key is q, exit the program. *)
  if key_pressed () then 
    match read_key () with
    | 'q' -> exit 0
    | _   -> printf "Invalid Input\n";

    animate point velocity
  else begin

      let new_point = { x = point. x + velocity. x; y = point. y + velocity. y } in
      let new_lead_point = leading_point new_point velocity in
      (* If either x or y is outside of the window, then call bounce. 
     else just call animate again with the new point. *)
      if new_lead_point. x > 640 || 
         new_lead_point. y > 480 || 
         new_lead_point. x < 0 || 
         new_lead_point. y < 0 then
        let bounce = bounce new_point velocity in
        let bounce_point = hd bounce in
        let bounce_velocity = hd (tl bounce) in
        animate bounce_point bounce_velocity

      else 
        animate new_point velocity
  end;;

   
animate { x = 0; y = 0} { x = 10; y = 10 };;
close_graph ();;
